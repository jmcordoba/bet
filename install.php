<?php

// incluimos clases
include 'clases/bbdd.php';
include 'clases/partido.php';

// recojemos la orden de la URL
$accion = $_GET['accion'];
$temp   = $_GET['temp'];

// dependiendo de la accion
switch ($accion)
{
    //
    case 'install':

        // incluimos le nombre de los ficheros CSV
        $ficheros = array("2014_2015",
            "2013_2014",
            "2012_2013",
            "2011_2012",
            "2010_2011",
            "2009_2010",
            "2008_2009",
            "2007_2008",
            "2006_2007",
            "2005_2006");

        // por cada uno de los ficheros
        for($f=0;$f<count($ficheros);$f++) {

            // inicializamos el numero de ficla
            $fila = 1;

            // si podemos abrir el fichero en modo lectura
            if (($gestor = fopen("laliga/".$ficheros[$f].".csv", "r")) !== FALSE) {

                // por cada una de las ficlas del CSV
                while ( ($datos = fgetcsv($gestor, 0, ",")) !== FALSE) {

                    // obtenemos el numero de columnas
                    $numero = count($datos);

                    // si nos saltamos la primera fila en la que aparecen los nombres de
                    // las columnas
                    if($fila>1) {

                        // creamos un objeto de la clase partido
                        $auxPartido = new Partido();

                        // inicializamos el objeto con cada uno de los partidos
                        $auxPartido->temp = (string)$ficheros[$f];
                        $auxPartido->Div = (string)$datos[0];
                        $auxPartido->Date = (string)$datos[1];
                        
                        $fecha = explode('/', $auxPartido->Date);
                        $year  = "20".$fecha[2];
                        $month = $fecha[1];
                        $day   = $fecha[0];
                        $auxPartido->DateTime = "$year-$month-$day 00:00:00";
                        
                        $auxPartido->HomeTeam = (string)$datos[2];
                        $auxPartido->AwayTeam = (string)$datos[3];
                        $auxPartido->FTHG = (string)$datos[4];
                        $auxPartido->FTAG = (string)$datos[5];
                        $auxPartido->FTR = (string)$datos[6];
                        $auxPartido->HTHG = (string)$datos[7];
                        $auxPartido->HTAG = (string)$datos[8];
                        $auxPartido->HTR = (string)$datos[9];
                        $auxPartido->HS = (string)$datos[10];
                        $auxPartido->AS = (string)$datos[11];
                        $auxPartido->HST = (string)$datos[12];
                        $auxPartido->AST = (string)$datos[13];
                        $auxPartido->HF = (string)$datos[14];
                        $auxPartido->AF = (string)$datos[15];
                        $auxPartido->HC = (string)$datos[16];
                        $auxPartido->AC = (string)$datos[17];
                        $auxPartido->HY = (string)$datos[18];
                        $auxPartido->AY = (string)$datos[19];
                        $auxPartido->HR = (string)$datos[20];
                        $auxPartido->AR = (string)$datos[21];
                        $auxPartido->B365H = (string)$datos[22];
                        $auxPartido->B365D = (string)$datos[23];
                        $auxPartido->B365A = (string)$datos[24];
        
                        $riesgo = $auxPartido->calculaPorcentajePartido(
                            $auxPartido->B365H, $auxPartido->B365D, $auxPartido->B365A);

                        $auxPartido->riesgo = (string)$riesgo;
                        
                        // guardamos el partido en la tabla
                        $auxPartido->guardaPartido();
                    }

                    // aumentamos el numero de la fila
                    $fila++;
                }

                // cerramos el handle al archivo
                fclose($gestor);
            }
            
            echo $ficheros[$f].'.csv introducido en la bbdd.<br>';
        }
        
        break;
    
    //
    case 'update':

        // incluimos le nombre de los ficheros CSV
        $tempActual = "2014_2015";

        // creamos un objeto de la clase partido
        $partido = new Partido();
        // 
        $partido->borrarPartidos($tempActual);

        // inicializamos el numero de ficla
        $fila = 1;

        // si podemos abrir el fichero en modo lectura
        if (($gestor = fopen("laliga/".$tempActual.".csv", "r")) !== FALSE) {

            // por cada una de las ficlas del CSV
            while ( ($datos = fgetcsv($gestor, 0, ",")) !== FALSE) {

                // obtenemos el numero de columnas
                $numero = count($datos);

                // si nos saltamos la primera fila en la que aparecen los nombres de
                // las columnas
                if($fila>1) {

                    // creamos un objeto de la clase partido
                    $auxPartido = new Partido();

                    // inicializamos el objeto con cada uno de los partidos
                    $auxPartido->temp = (string)$tempActual;
                    $auxPartido->Div = (string)$datos[0];
                    $auxPartido->Date = (string)$datos[1];
                    $auxPartido->HomeTeam = (string)$datos[2];
                    $auxPartido->AwayTeam = (string)$datos[3];
                    $auxPartido->FTHG = (string)$datos[4];
                    $auxPartido->FTAG = (string)$datos[5];
                    $auxPartido->FTR = (string)$datos[6];
                    $auxPartido->HTHG = (string)$datos[7];
                    $auxPartido->HTAG = (string)$datos[8];
                    $auxPartido->HTR = (string)$datos[9];
                    $auxPartido->HS = (string)$datos[10];
                    $auxPartido->AS = (string)$datos[11];
                    $auxPartido->HST = (string)$datos[12];
                    $auxPartido->AST = (string)$datos[13];
                    $auxPartido->HF = (string)$datos[14];
                    $auxPartido->AF = (string)$datos[15];
                    $auxPartido->HC = (string)$datos[16];
                    $auxPartido->AC = (string)$datos[17];
                    $auxPartido->HY = (string)$datos[18];
                    $auxPartido->AY = (string)$datos[19];
                    $auxPartido->HR = (string)$datos[20];
                    $auxPartido->AR = (string)$datos[21];
                    $auxPartido->B365H = (string)$datos[22];
                    $auxPartido->B365D = (string)$datos[23];
                    $auxPartido->B365A = (string)$datos[24];
                    
                    $riesgo = $auxPartido->calculaPorcentajePartido(
                        $auxPartido->B365H, $auxPartido->B365D, $auxPartido->B365A);
                    
                    $auxPartido->riesgo = (string)$riesgo;
                    
                    // guardamos el partido en la tabla
                    $auxPartido->guardaPartido();
                }

                // aumentamos el numero de la fila
                $fila++;
            }

            // cerramos el handle al archivo
            fclose($gestor);
        }

        echo $tempActual.'.csv actualizado en la bbdd.<br>';
        
        break;
        
    case 'remove':
        
        // creamos un objeto de la clase partido
        $partido = new Partido();
        // 
        $partido->vaciarTablaPartidos();
        //
        echo 'tabla <b>bet_partidos</b> vaciada.';
        // 
        break;
        
    case 'calculate':
        
        // creamos un objeto de la clase partido
        $partido = new Partido();
        // 
        $res = $partido->calculaClasTemp($temp);
        //
        echo "Temporada $temp calculada: $res";
        // 
        break;
    
    default:
        
        //
        echo "No existe la orden <b>$accion</b> introducida.";
        //
        break;
}