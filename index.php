<?php

// inicializamos
require_once 'inicio.php';

// obtenemos valores de la URL
$pagina = $_REQUEST['pagina'] ? $_REQUEST['pagina'] : '';
$accion = $_REQUEST['accion'] ? $_REQUEST['accion'] : '';
$team1  = $_REQUEST['team1']  ? $_REQUEST['team1']  : '';
$team2  = $_REQUEST['team2']  ? $_REQUEST['team2']  : '';
$temp   = $_REQUEST['temp']   ? $_REQUEST['temp']   : '';

// inicializamos array de valores de template
$valores = array();

// dependiendo de la pagina solicitada
if($pagina=='equipo') {
    
    // obtenemos la lista de equipos
    $equipo = new Equipo($team1);
    $aEquipos = $equipo->obtenEquipos();
    
    //
    $temporada = new Temporada();
    $aTemporadas = $temporada->buscaAllTemps();
    
    //
    $equipo->getDatos($temp);
    
    //
    $valores['aEquipos'] = $aEquipos;
    $valores['accion'] = $accion;
    $valores['pagina'] = $pagina;
    $valores['team1'] = $team1;
    $valores['temp'] = $temp;
    $valores['aTemporadas'] = $aTemporadas;
    $valores['temporada'] = $temporada;
    
    // mostramos el template
    echo $twig->render('equipo.twig', $valores);
}
elseif($pagina=='encuentros') {
    
    // obtenemos la lista de equipos
    $equipo = new Equipo();
    $aEquipos = $equipo->obtenEquipos();

    //
    if($accion=='buscar' && $team1!='' && $team2!='') {

        //
        $partido = new Partido();
        $listaPartidos = $partido->buscarPartidosHT($team1,$team2);
    }
    
    //
    $valores['aEquipos'] = $aEquipos;
    $valores['accion'] = $accion;
    $valores['pagina'] = $pagina;
    $valores['team1'] = $team1;
    $valores['team2'] = $team2;
    $valores['listaPartidos'] = $listaPartidos;

    // mostramos el template
    echo $twig->render('encuentros.twig', $valores);
}
elseif($pagina=='temporada') {
    
    //
    $temporada = new Temporada($temp);
    $aTemporadas = $temporada->buscaAllTemps();
    
    //
    $temporada->getDatos($temp);
  
    //
    $valores['temp'] = $temp;
    $valores['aTemporadas'] = $aTemporadas;
    $valores['temporada'] = $temporada;
    
    // mostramos el template
    echo $twig->render('temporada.twig', $valores);
}
else {
    //
    $valores['accion'] = $accion;
    $valores['pagina'] = $pagina;

    // mostramos el template
    echo $twig->render('index.twig', $valores);
}