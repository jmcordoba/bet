<?php

// cargamos Twig, ruta de plantillas y determinamos ruta de cache
require_once 'twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register(true);
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment(
				$loader/*, array(
                    'cache' => 'public/cache',
				)*/
		  );

// incluimos clases
require_once 'clases/bbdd.php';
require_once 'clases/partido.php';
require_once 'clases/equipo.php';
require_once 'clases/temporada.php';
require_once 'clases/funciones.php';

// inicializamos la clase de funciones
$fun = new Funciones();