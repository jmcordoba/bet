<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * http://www.football-data.co.uk/spainm.php
 */

/**
 * Description of Partido
 *
 * @author root
 */
class Partido extends Bbdd{
    
    public $temp = '';      // Temporada (por ejemplo: 2013-2014)
    public $Div = '';       // League Division
    public $Date = '';      // Match Date (dd/mm/yy)
    public $DateTime = '';  // 
    public $HomeTeam = '';  // Home Team
    public $AwayTeam = '';  // Away Team
    public $FTHG = '';      // Full Time Home Team Goals
    public $FTAG = '';      // Full Time Away Team Goals
    public $FTR = '';       // Full Time Result (H=Home Win, D=Draw, A=Away Win)
    public $HTHG = '';      // Half Time Home Team Goals
    public $HTAG = '';      // Half Time Away Team Goals
    public $HTR = '';       // Half Time Result (H=Home Win, D=Draw, A=Away Win)
    public $HS = '';        // Home Team Shots
    public $AS = '';        // Away Team Shots
    public $HST = '';       // Home Team Shots on Target
    public $AST = '';       // Away Team Shots on Target
    public $HF = '';        // Home Team Fouls Committed
    public $AF = '';        // Away Team Fouls Committed
    public $HC = '';        // Home Team Corners
    public $AC = '';        // Away Team Corners
    public $HY = '';        // Home Team Yellow Cards
    public $AY = '';        // Away Team Yellow Cards
    public $HR = '';        // Home Team Red Cards
    public $AR = '';        // Away Team Red Cards
    public $B365H = '';     // Bet365 home win odds
    public $B365D = '';     // Bet365 draw odds
    public $B365A = '';     // Bet365 away win odds
    public $riesgo = '';    // 
    
    /**
     * 
     */
    function __constructor() {}
    
    /**
     * 
     */
    public function guardaPartido()
    {
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        
        //
        $sql  = " INSERT INTO `bet_partidos` (`temp`,
        `Div`,
        `Date`,
        `DateTime`,
        `HomeTeam`,
        `AwayTeam`,
        `FTHG`,
        `FTAG`,
        `FTR`,
        `HTHG`,
        `HTAG`,
        `HTR`,
        `HS`,
        `AS`,
        `HST`,
        `AST`,
        `HF`,
        `AF`,
        `HC`,
        `AC`,
        `HY`,
        `AY`,
        `HR`,
        `AR`,
        `B365H`,
        `B365D`,
        `B365A`,
        `riesgo`) ";
        $sql .= " VALUES ("
            . "'$this->temp',"
            . "'$this->Div',"
            . "'$this->Date',"
            . "'$this->DateTime',"
            . "'$this->HomeTeam',"
            . "'$this->AwayTeam',"
            . "'$this->FTHG',"
            . "'$this->FTAG',"
            . "'$this->FTR',"
            . "'$this->HTHG',"
            . "'$this->HTAG',"
            . "'$this->HTR',"
            . "'$this->HS',"
            . "'$this->AS',"
            . "'$this->HST',"
            . "'$this->AST',"
            . "'$this->HF',"
            . "'$this->AF',"
            . "'$this->HC',"
            . "'$this->AC',"
            . "'$this->HY',"
            . "'$this->AY',"
            . "'$this->HR',"
            . "'$this->AR',"
            . "'$this->B365H',"
            . "'$this->B365D',"
            . "'$this->B365A',"
            . "'$this->riesgo'"
            . ");";
        
        //
        $result = mysql_query($sql, $link);
        
        //
        mysql_close($link);
    }
    
    /**
     * 
     */
    public function vaciarTablaPartidos()
    {
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql  = "truncate table bet_partidos";
        //
        $result = mysql_query($sql, $link);
        //
        mysql_close($link);
    }
    
    /**
     * 
     */
    public function borrarPartidos($temporada)
    {
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql = "delete from bet_partidos where temp='$temporada' ";
        //
        $result = mysql_query($sql, $link);
        //
        mysql_close($link);
    }
    
    /**
     * 
     * @param type $team1
     * @param type $team2
     */
    public function buscarPartidosHT($team1,$team2)
    {
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql  = "select * from bet_partidos "
            . "where (HomeTeam='$team1' and AwayTeam='$team2') "
            . "or (HomeTeam='$team2' and AwayTeam='$team1') order by DateTime desc";
        //
        $result = mysql_query($sql, $link);
        //
        $matches = array();
        $match = array();
        // por cada resultado
        while($row = mysql_fetch_array($result)) {
            // introducimos al equipo en el array de equipos
            $match['temp']     = (string)$row['temp'];
            $match['Div']      = (string)$row['Div'];
            $match['Date']     = (string)$row['Date'];
            $match['DateTime'] = (string)$row['DateTime'];
            $match['HomeTeam'] = (string)$row['HomeTeam'];
            $match['AwayTeam'] = (string)$row['AwayTeam'];
            $match['PosLiga']  = (string)$row['PosLiga'];
            $match['FTHG']     = (string)$row['FTHG'];
            $match['FTAG']     = (string)$row['FTAG'];
            $match['FTR']      = (string)$row['FTR'];
            $match['HTHG']     = (string)$row['HTHG'];
            $match['HTAG']     = (string)$row['HTAG'];
            $match['HTR']      = (string)$row['HTR'];
            $match['HS']       = (string)$row['HS'];
            $match['AS']       = (string)$row['AS'];
            $match['HST']      = (string)$row['HST'];
            $match['AST']      = (string)$row['AST'];
            $match['HF']       = (string)$row['HF'];
            $match['AF']       = (string)$row['AF'];
            $match['HC']       = (string)$row['HC'];
            $match['AC']       = (string)$row['AC'];
            $match['HY']       = (string)$row['HY'];
            $match['AY']       = (string)$row['AY'];
            $match['HR']       = (string)$row['HR'];
            $match['AR']       = (string)$row['AR'];
            $match['B365H']    = (string)$row['B365H'];
            $match['B365D']    = (string)$row['B365D'];
            $match['B365A']    = (string)$row['B365A'];
            $match['riesgo']   = (string)$row['riesgo'];
            // guardamos el partido
            $matches[] = $match;
        }
        //
        mysql_free_result($result);
        //
        mysql_close($link);
        // devolvemos el array de equipos
        return $matches;
    }
    
    /**
     * 
     * @param type $b365h
     * @param type $b365d
     * @param type $b365a
     * @return type
     */
    function calculaPorcentajePartido($b365h,$b365d,$b365a)
    {
        //
        $resH = floatval(1/floatval($b365h));
        $resD = floatval(1/floatval($b365d));
        $resA = floatval(1/floatval($b365a));
        //
        return number_format( 
            floatval( 
                number_format( 1/( $resH + $resD + $resA )
                , 4)
            ) 
        ,4);
    }
    
    /**
     * 
     * @param type $temp
     */
    public function calculaClasTemp($temp)
    {
        /*

        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql = "select * from bet_partidos "
            . "where temp='$temp' "
            . "order by DateTime desc";
        //
        $result = mysql_query($sql, $link);
        
        //
        $partidos = 0;
        
        //
        $team = array();
        
        // por cada resultado
        while($row = mysql_fetch_array($result)) {
            
            // si el equipo NO esta en el array como indice
            if( !array_key_exists($row["HomeTeam"], $team) ) {
                
                // introducimos al equipo en el array de equipos
                $teamProvi[ $row["HomeTeam"] ] = 0;
                $teamFinal[ $row["HomeTeam"] ] = 0;
            }
            
            //
            $partidos++;
        }
        
        
        //print_r($team);die();
        
        
        $fechaAnterior = '';
        $fechaActual   = '';
        
        //
        $result = mysql_query($sql, $link);
        
        // por cada resultado
        while($row = mysql_fetch_array($result)) {
            
            $fechaActual = strtotime($row["DateTime"]);
            
            if($fechaActual!=$fechaAnterior) {
                
                // traspasa los datos de la clasificacion provisional a la general
                foreach(array_keys($teamProvi) as $key){
                    
                    $teamFinal[$key] = $teamFinal[$key] + $teamProvi[$key];
                }
                
                // recalcula posiciones en la clasificacion
                
                
                
                
                // guarda las posiciones recalculadas
                
                
                
                
                // vacia los datos de la clasificacion provisional
                foreach(array_keys($teamProvi) as $key){
                    
                    $teamProvi[$key] = 0;
                }
                
                // actualiza la marca de la fecha
                $fechaAnterior = $fechaActual;
            }
            
            // victoria local
            if( $row["FTHG"] > $row["FTAG"] ) {
                
                $teamProvi[ $row["HomeTeam"] ] = $teamProvi[ $row["HomeTeam"] ] + 3;
            }
            // victoria visitante
            elseif ( $row["FTHG"] < $row["FTAG"] ) {
                
                $teamProvi[ $row["AwayTeam"] ] = $teamProvi[ $row["AwayTeam"] ] + 3;
            }
            // empate
            else {
                $teamProvi[ $row["HomeTeam"] ]++;
                $teamProvi[ $row["AwayTeam"] ]++;
            }
        }
        
        arsort($teamProvi);
        arsort($teamFinal);
        
        //print_r($teamProvi);die();
        print_r($teamFinal);die();
        
        
        
        
        
        
        //
        mysql_free_result($result);
        //
        mysql_close($link);
        // devolvemos el array de equipos
        return $partidos;
        */
        
        return true;
    }
}
