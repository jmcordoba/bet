<?php

class Temporada extends Bbdd{
    
    public $temp = '';
    
    public $todas = array();
    
    // victorias
    public $vicLocal = 0;
    public $vicEmpat = 0;
    public $vicVisit = 0;
    
    // goles
    public $golLocal = 0;
    public $golVisit = 0;
    public $golTotal = 0;
    
    // cuotas
    public $cuoLocal = array();
    public $cuoEmpat = array();
    public $cuoVisit = array();
    
    //
    public $aResSum = array();
    public $aResSumProb = array();
    public $aResult = array();
    public $aResultProb = array();
    
    // riesgos
    public $riesgo = 0;
    
    /**
     * 
     * @param type $temp
     */
    function __constructor($temp) {
        
        $this->temp = $temp;
    }
    
    /**
     * 
     * @return type
     */
    public function buscaAllTemps() {
        
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql  = "select distinct temp from bet_partidos order by DateTime desc";
        //
        $result = mysql_query($sql, $link);
        //
        $temp = array();
        // por cada resultado
        while($row = mysql_fetch_array($result)) {
            // introducimos al equipo en el array de equipos
            $temp[] = (string)$row['temp'];
        }
        //
        mysql_free_result($result);
        //
        mysql_close($link);
        //
        unset($link);
        //
        sort($temp);
        //
        return $temp;
    }
    
    /*
     * numero de victorias locales, empates y victorias visitantes
     * numero de goles locales, goles visitantes y goles totales
     * cuotas ganadoras como local, empate, visitante
     */
    
    /**
     * 
     * @param type $temp
     * @return boolean
     */
    public function getDatos($temp) {
        
        //
        $res = false;
        //
        if($temp!='') {
            
            $this->temp = $temp;
            
            if($temp=='todas') {
                
                $temps = $this->buscaAllTemps();

                for($t=1;$t<count($temps);$t++) {
                
                    $temp = $temps[$t];
                    
                    $auxTemp = new Temporada();
                    $auxTemp->temp = $temp;
                    
                    $auxTemp->getVictoriasHDA($temp);
                    $auxTemp->getGolesHDA($temp);
                    
                    $this->todas[] = $auxTemp;
                }
                
                for($t=0;$t<count($this->todas);$t++) {
                
                    $this->vicLocal += $this->todas[$t]->vicLocal;
                    $this->vicEmpat += $this->todas[$t]->vicEmpat;
                    $this->vicVisit += $this->todas[$t]->vicVisit;
                    
                    $this->golLocal += intval($this->todas[$t]->golLocal);
                    $this->golEmpat += intval($this->todas[$t]->golEmpat);
                    $this->golVisit += intval($this->todas[$t]->golVisit);
                }
                    
                $this->vicLocal = number_format( 
                    (intval($this->vicLocal)/intval(count($this->todas))), 
                    2);
                $this->vicEmpat = number_format( 
                    (intval($this->vicEmpat)/intval(count($this->todas))), 
                    2);
                $this->vicVisit = number_format( 
                    (intval($this->vicVisit)/intval(count($this->todas))), 
                    2);
                
                $this->golLocal = number_format( 
                    (intval($this->golLocal)/intval(count($this->todas))), 
                    2);
                $this->golEmpat = number_format( 
                    (intval($this->golEmpat)/intval(count($this->todas))), 
                    2);
                $this->golVisit = number_format( 
                    (intval($this->golVisit)/intval(count($this->todas))), 
                    2);
            }
            else {
            
                $this->getVictoriasHDA($temp);
                $this->getGolesHDA($temp);
                $this->getRiesgo($temp);
                $this->getResultadosSumados($temp);
                $this->getResultados($temp);
            }
            //
            $res = true;
        }
        //
        return $res;
    }
    
    /**
     * 
     * @param type $temp
     * @return boolean
     */
    public function getVictoriasHDA($temp) {
        
        //
        $res = false;
        //
        if($temp!='') {
            // obtenemos handler de la bbdd
            $link = $this->Conectarse();
            //
            $sql  = "select FTR from bet_partidos where temp='$temp'";
            //
            $result = mysql_query($sql, $link);
            // por cada resultado
            while($row = mysql_fetch_array($result)) {
                //
                switch ($row['FTR']) {
                    
                    case "H":
                        $this->vicLocal = $this->vicLocal + 1;
                        break;
                    case "D":
                        $this->vicEmpat = $this->vicEmpat + 1;
                        break;
                    case "A":
                        $this->vicVisit = $this->vicVisit + 1;
                        break;
                }
            }
            //
            $res = true;
            //
            mysql_free_result($result);
            //
            mysql_close($link);
            //
            unset($link);
        }
        //
        return $res;
    }
    
    /**
     * 
     * @param type $temp
     * @return boolean
     */
    public function getGolesHDA($temp)
    {
        //
        $res = false;
        //
        if($temp!='') {
            // obtenemos handler de la bbdd
            $link = $this->Conectarse();
            //
            $sql = "select FTHG,FTAG from bet_partidos where temp='$temp'";
            //
            $result = mysql_query($sql, $link);
            // por cada resultado
            while($row = mysql_fetch_array($result)) {
                
                $this->golLocal = intval($this->golLocal + $row['FTHG']);
                $this->golVisit = intval($this->golVisit + $row['FTAG']);
                $this->golTotal = intval($this->golLocal) + intval($this->golVisit);
            }
            //
            $res = true;
            //
            mysql_free_result($result);
            //
            mysql_close($link);
            //
            unset($link);
        }
        //
        return $res;
    }
    
    /**
     * 
     * @param type $temp
     * @return boolean
     */
    public function getRiesgo($temp)
    {
        //
        $res = false;
        $cont = 0;
        //
        if($temp!='') {
            // obtenemos handler de la bbdd
            $link = $this->Conectarse();
            //
            $sql = "select riesgo from bet_partidos where temp='$temp'";
            //
            $result = mysql_query($sql, $link);
            // por cada resultado
            while($row = mysql_fetch_array($result)) {
                // obtenemos el riesgo
                $this->riesgo += floatval($row['riesgo']);
                // incrementamos el contador
                $cont++;
            }
            // obtenemos la media de riesgo de toda la temporada
            $this->riesgo = number_format( floatval($this->riesgo/$cont), 4);
            // ok
            $res = true;
            //
            mysql_free_result($result);
            //
            mysql_close($link);
            //
            unset($link);
        }
        //
        return $res;
    }
    
    /**
     * 
     * @param type $temp
     * @return type
     */
    public function getResultados($temp)
    {
        //
        $result = false;
        $aLista = array();
        //
        if($temp!='') {
            // obtenemos handler de la bbdd
            $link = $this->Conectarse();
            //
            $sql = "select * from bet_partidos where temp='$temp'";
            //
            $result = mysql_query($sql, $link);
            // por cada resultado
            while($row = mysql_fetch_array($result))
            {
                //
                $key = $row['FTHG']."a".$row['FTAG'];
                //
                if( !array_key_exists ($key, $aLista) ) {
                    //
                    $aLista[$key] = 1;
                }
                else {
                    //
                    $aLista[$key] = $aLista[$key]+1;
                }
            }
            //
            mysql_free_result($result);
            //
            mysql_close($link);
            //
            unset($link);
        }
        //
        arsort($aLista);
        //
        $this->aResult = $aLista;
        //
        $this->getProbResult($temp);
        //
        return $result;
    }
    
    /**
     * 
     * @param type $temp
     */
    public function getProbResult($temp)
    {
        //
        $total = array_sum($this->aResult);
        //
        foreach($this->aResult as $key => $value)
        {
            //
            $this->aResultProb[$key] = number_format( 
                ( floatval($value/$total) )*100, 
                2);
        }
        //
        return true;
    }
    
    /**
     * 
     * @param type $temp
     * @return type
     */
    public function getResultadosSumados($temp)
    {
        //
        $result = false;
        $aLista = array();
        //
        if($temp!='') {
            // obtenemos handler de la bbdd
            $link = $this->Conectarse();
            //
            $sql = "select * from bet_partidos where temp='$temp'";
            //
            $result = mysql_query($sql, $link);
            // por cada resultado
            while($row = mysql_fetch_array($result))
            {
                //
                $key = intval($row['FTHG'])+intval($row['FTAG']);
                //
                if( !array_key_exists ($key, $aLista) ) {
                    //
                    $aLista[$key] = 1;
                }
                else {
                    //
                    $aLista[$key] = $aLista[$key]+1;
                }
            }
            //
            mysql_free_result($result);
            //
            mysql_close($link);
            //
            unset($link);
        }
        //
        ksort($aLista);
        //
        $this->aResSum = $aLista;
        //
        $this->getProbResSum($temp);
        //
        return $result;
    }
    
    /**
     * 
     * @param type $temp
     */
    public function getProbResSum($temp)
    {
        //
        $total = array_sum($this->aResSum);
        //
        foreach($this->aResSum as $key => $value)
        {
            //
            $this->aResSumProb[$key] = number_format( 
                ( floatval($value/$total) )*100, 
                2);
        }
        //
        return true;
    }
}
