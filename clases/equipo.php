<?php
/**
 * 
 */
class Equipo extends Bbdd {
    
    public $team1 = '';
    public $team2 = '';
    
    /**
     * 
     * @param type $team1
     * @param type $team2
     */
    function __construct($team1='',$team2='') {
        
        $this->team1 = $team1;
        $this->team2 = $team2;
    }
    
    /**
     * 
     * @return type
     */
    function obtenEquipos()
    {
        // obtenemos handler de la bbdd
        $link = $this->Conectarse();
        //
        $sql  = "select HomeTeam from bet_partidos";
        //
        $result = mysql_query($sql, $link);
        //
        $team = array();
        // por cada resultado
        while($row = mysql_fetch_array($result)) {
            // si el equipo NO esta en el array
            if( !in_array($row["HomeTeam"], $team) ) {
                // introducimos al equipo en el array de equipos
                $team[] = (string)$row["HomeTeam"];
            }
        }
        // ordenamos los equipos por orden alfabetico
        sort($team);
        //
        mysql_free_result($result);
        //
        mysql_close($link);
        // devolvemos el array de equipos
        return $team;
    }
    
    /**
     * 
     * @param type $aTemp
     * @return boolean
     */
    public function getDatos($temp)
    {
        //
        $res = false;
        //
        if($temp!='')
        {
            //
            $this->getRacha($temp);
        }
        //
        return $res;
    }
    
    public function getRacha($temp)
    {
        
    }
}
