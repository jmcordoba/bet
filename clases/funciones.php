<?php

class Funciones {
    
    /**
     * 
     * @param type $type
     * @param type $name
     * @return type
     */
    public function getVar($type, $name) {        

        $res = '';
        
        if($type=='GET') {
            $res = filter_input(
                INPUT_GET, 
                $name, 
                FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        }
        else if($type=='POST') {
            $res = filter_input(
                INPUT_POST, 
                $name, 
                FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        }
        else if($type=='COOKIE') {
            $res = filter_input(
                INPUT_COOKIE, 
                $name, 
                FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        }
        
        return $res;
    }
}
